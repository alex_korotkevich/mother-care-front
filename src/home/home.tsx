import { Route, Routes } from "react-router-dom";
import { Logout } from "../auth/logout";
import { OpenTask } from "../tasks/open-task";
import { TaskList } from "../tasks/task-list";

export const Home = () => {
  return (
    <>
      <Logout />
      <Routes>
        <Route path="/" element={<TaskList />}></Route>
        <Route path="/:id" element={<OpenTask />}></Route>
      </Routes>
    </>
  );
};
