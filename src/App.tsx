import React from "react";
import { useSelector } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { TState } from ".";
import { Auth } from "./auth/auth";
import { Home } from "./home/home";

export function App() {
  const token = useSelector<TState, boolean>((state) => !!state.token);
  return <BrowserRouter>{token ? <Home /> : <Auth />}</BrowserRouter>;
}
