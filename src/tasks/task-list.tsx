import { useEffect, useState } from "react";
import { SeeButton } from "./buttons/task-see.button";
import { taskController } from "./task-controller";

type TChild = {
  id: number;
  name: string;
  login: string;
  password: string;
};

export const TaskList = () => {
  const [state, setState] = useState<any[]>([]);
  const [child, setChild] = useState<TChild>({
    id: 0,
    name: "",
    login: "",
    password: "",
  });

  const changeStatus = async (id: number) => {
    const task = state.find((item) => item.id === id);
    if (task) {
      const status = task.status === "complited" ? "in process" : "complited";
      await taskController.changeStatus(id, status);
      const newState = state.map((item) => {
        if (item.id === id) {
          item.status = status;
        }
        return item;
      });
      setState(newState);
    }
  };

  const loadTaskList = async () => {
    const tasks = await taskController.list();
    setState(tasks);
  };

  const loadChildName = async () => {
    const child = await taskController.findChildName();
    setChild(child);
  };

  useEffect(() => {
    loadTaskList();
  }, []);

  useEffect(() => {
    loadChildName();
  }, []);

  if (!state.length) {
    return (
      <>
        <div className="no_task_table">
          <h1 className="no_task">
            <strong>You haven't got any task</strong>
          </h1>
        </div>
      </>
    );
  }

  return (
    <>
      <ChildItem child={child} />
      <div className="task_table">
        <ol>
          {state.map((item) => (
            <TaskItem changeStatus={changeStatus} post={item} />
          ))}
        </ol>
      </div>
    </>
  );
};

type TTaskItemProps = {
  post: {
    id: number;
    title: string;
    text: string;
    status: string;
  };
  changeStatus: Function;
};
const TaskItem = (props: TTaskItemProps) => {
  return (
    <>
      <div className="every_task_table">
        <div>
          <strong className="title_status_task">TITLE:</strong>
          <span className="task_text">{props.post.title}</span>
        </div>
        <div>
          <strong className="title_status_task">STATUS:</strong>
          <strong className="task_text">{props.post.status}</strong>
        </div>
        <div>
          <SeeButton id={props.post.id} />{" "}
          <label className="input">
            <input
              type="checkbox"
              onChange={() => props.changeStatus(props.post.id)}
              defaultChecked={props.post.status === "complited"}
            />
            <strong className="done">DONE</strong>
          </label>
        </div>
      </div>
    </>
  );
};

type TChildItem = {
  child: TChild;
};

const ChildItem = (props: TChildItem) => {
  return (
    <>
      <div className="label">
        {" "}
        <h1>
          <strong className="task_list_label">TASK LIST</strong>
          <div>
            <strong className="for"> for:</strong>
            <strong className="name">{props.child.name}</strong>{" "}
          </div>
        </h1>
      </div>
    </>
  );
};
