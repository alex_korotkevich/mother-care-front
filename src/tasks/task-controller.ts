import axios from "axios";
const baseUrl = `${process.env.REACT_APP_BASE_URL}/child/tasks`;

class TaskController {
  list = async () => {
    try {
      const token = localStorage.getItem("token");
      const res = await axios.get(`${baseUrl}`, {
        headers: { authorization: token || "" },
      });
      return res.data;
    } catch (err) {
      localStorage.removeItem("token");
      throw err;
    }
  };

  find = async (id: number) => {
    const token = localStorage.getItem("token");
    const res = await axios.get(`${baseUrl}/${id}`, {
      headers: { authorization: token || "" },
    });
    return res.data;
  };

  findChildName = async () => {
    const token = localStorage.getItem("token");
    console.log(token);
    const res = await axios.get(`${baseUrl}/me`, {
      headers: { authorization: token || "" },
    });
    return res.data;
  };

  changeStatus = async (id: number, status: string) => {
    await axios.put(`${baseUrl}/${id}`, { status });
  };
}

export const taskController = new TaskController();
