import { useNavigate } from "react-router-dom";

type TSeeButton = {
  id: number;
};

export const SeeButton = (props: TSeeButton) => {
  const navigate = useNavigate();

  const seeMore = async () => {
    navigate(`/${props.id}`);
  };

  return (
    <>
      <button className="buttonMore" onClick={seeMore}>
        <strong>
          {" "}
          <span>More</span>
        </strong>
      </button>
    </>
  );
};
