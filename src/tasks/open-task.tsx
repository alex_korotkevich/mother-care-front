import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { ReturnButton } from "./buttons/return-button";
import { taskController } from "./task-controller";

type TTask = {
  id: number;
  title: string;
  text: string;
  status: string;
  userId: number;
  childId: number;
};

type TChild = {
  id: number;
  name: string;
  login: string;
  password: string;
  userId: number;
};

export const OpenTask = () => {
  const [task, setTask] = useState<TTask>({
    id: 0,
    title: "",
    text: "",
    status: "",
    userId: 0,
    childId: 0,
  });

  const [child, setChild] = useState<TChild>({
    id: 0,
    name: "",
    login: "",
    password: "",
    userId: 0,
  });

  const params: any = useParams();

  const changeStatus = async (task: TTask) => {
    const status = task.status === "complited" ? "in process" : "complited";
    await taskController.changeStatus(params.id, status);
    setTask({ ...task, status });
  };

  const loadFind = async () => {
    const task = await taskController.find(params.id);
    setTask(task);
  };

  const loadChildName = async () => {
    const child = await taskController.findChildName();
    setChild(child);
  };

  useEffect(() => {
    loadFind();
  }, []);

  useEffect(() => {
    loadChildName();
  }, []);

  return (
    <>
      {" "}
      <ReturnButton />
      <ChildItem child={child} />
      <FindItem changeStatus={changeStatus} task={task} />
    </>
  );
};

type TFindItem = {
  task: TTask;
  changeStatus: Function;
};

const FindItem = (props: TFindItem) => {
  return (
    <>
      <div className="open_task_table">
        <div>
          <strong className="open_task_title_status">TITLE:</strong>
          <strong className="task_text"> {props.task.title}</strong>
        </div>
        <div>
          <strong className="open_task_text">TASK:</strong>
          <strong className="task_text">{props.task.text}</strong>
        </div>
        <div>
          <strong className="open_task_title_status">STATUS:</strong>{" "}
          <strong className="task_text"> {props.task.status}</strong>
        </div>
        <div>
          <label>
            <div className="open_task_input_table">
              <input
                type="checkbox"
                onChange={() => props.changeStatus(props.task)}
                defaultChecked={props.task.status === "complited"}
              />
              <strong className="done"> DONE</strong>
            </div>
          </label>
        </div>
      </div>
    </>
  );
};

type TChildItem = {
  child: TChild;
};

const ChildItem = (props: TChildItem) => {
  return (
    <>
      <div>
        <strong className="for">for: </strong>
        <strong className="name">{props.child.name}</strong>
      </div>
    </>
  );
};
