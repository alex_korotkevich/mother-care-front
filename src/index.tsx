import { configureStore } from "@reduxjs/toolkit";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { ActionType } from "./action-type.enum";
import { App } from "./App";

export type TState = {
  token: string;
};

const defaultState: TState = {
  token: localStorage.getItem("token") || "",
};

const reducer = (
  state = defaultState,
  action: { type: ActionType; payload: any }
) => {
  switch (action.type) {
    case ActionType.setToken:
      localStorage.setItem("token", action.payload);
      return { ...state, token: action.payload };
    case ActionType.removeToken:
      localStorage.removeItem("token");
      return { ...state, token: "" };
    default:
      return state;
  }
};

const store = configureStore({ reducer });

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
