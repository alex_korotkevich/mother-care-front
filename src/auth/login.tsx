import { useState } from "react";
import { useDispatch } from "react-redux";
import { ActionType } from "../action-type.enum";
import { authController } from "./auth.controller";

export const Login = () => {
  const dispatch = useDispatch();

  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [color, setColor] = useState("");

  const loginButton = async () => {
    try {
      const token = await authController.login(login, password);
      if (!login || !password) {
        return setColor("red");
      }
      setColor("");
      dispatch({ type: ActionType.setToken, payload: token });
    } catch (err) {
      setColor("red");
    }
  };

  return (
    <>
      <div>
        <div className="login_table">
          {" "}
          <label>
            <strong className="login_password_text">LOGIN: </strong>
            <input
              className="input_login_password "
              value={login}
              onChange={(event) => setLogin(event.target.value)}
            ></input>
          </label>
        </div>
        <div className="password_table ">
          <label>
            <strong className="login_password_text">PASSWORD: </strong>
            <input
              className="input_login_password "
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            ></input>
          </label>
        </div>

        <button
          className="buttonLogin"
          style={{ background: color }}
          onClick={loginButton}
        >
          <strong>
            {" "}
            <span>LOGIN</span>
          </strong>
        </button>
      </div>
    </>
  );
};
