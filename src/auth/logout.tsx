import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { ActionType } from "../action-type.enum";
import { authController } from "./auth.controller";

export const Logout = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const logout = async () => {
    await authController.logout();
    dispatch({ type: ActionType.removeToken });
    navigate(`/`);
  };

  return (
    <button className="buttonLogout" onClick={logout}>
      <strong>LOGOUT</strong>
    </button>
  );
};
