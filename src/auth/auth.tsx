import { Route, Routes } from "react-router-dom";
import { Login } from "./login";

export const Auth = () => {
  return (
    <Routes>
      <Route path="/" element={<Login />}></Route>
    </Routes>
  );
};
