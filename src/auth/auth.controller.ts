import axios from "axios";
const baseUrl = `${process.env.REACT_APP_BASE_URL}/child`;

class AuthController {
  login = async (login: string, password: string) => {
    try {
      const res = await axios.post(`${baseUrl}/login`, { login, password });
      return res.data;
    } catch (err) {
      localStorage.removeItem("token");
      throw err;
    }
  };

  logout = async () => {
    try {
      const token = localStorage.getItem("token");
      // console.log(token);
      await axios.get(`${baseUrl}/logout`, {
        headers: { authorization: token || "" },
      });
    } catch (err) {
      localStorage.removeItem("token");
      throw err;
    }
  };
}
export const authController = new AuthController();
